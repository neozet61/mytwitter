<?php

namespace App\Http\Controllers;

use App\Models\Like;
use App\Models\Tweet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function GuzzleHttp\Promise\all;

class TweetController extends Controller
{
    public function showTweets()
    {
        if (!Auth::check()) {
            http_response_code(401);
            return redirect()->to('/');
        }

        $user = Auth::user();

        $likes = Like::query()
            ->where('user_id', '=', $user->id)
            ->get();

        $likedTweetsIds = $likes->pluck('tweet_id')->all();

        $tweets = Tweet::query()
            ->whereIn('user_id', $user->following->pluck('id'))
            ->orWhere('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->paginate(5);

        return view('/home-feed', [
            'tweets' => $tweets,
            'likedTweetsIds' => $likedTweetsIds
        ]);
    }

    public function createTweet(Request $request)
    {
        if (!Auth::check()) {
            http_response_code(401);
            return redirect()->to('/');
        }

        $request->validate([
            "newTweet" => 'required|string|min:1|max:250',
        ]);

        $tweet = new Tweet();
        $tweet->user_id = Auth::user()->id;
        $tweet->content = $request->newTweet;
        $tweet->save();

        return redirect()->to('/');
    }
}
