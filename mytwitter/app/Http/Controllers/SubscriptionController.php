<?php

namespace App\Http\Controllers;

use App\Models\Subscription;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubscriptionController extends Controller
{
    public function showFollower(Request $request)
    {
        if (!Auth::check()) {
            http_response_code(401);
            return redirect()->to('/');
        }

        $request->validate([
            "id" => 'required|integer|exists:App\Models\User,id'
        ]);

        $user = User::query()->find($request->id);

        $followers = $user->followers;

        return view('/follower', [
            'subscritons' => $followers,
            'followers' => 1
        ]);
    }

    public function showFollowing(Request $request)
    {
        if (!Auth::check()) {
            http_response_code(401);
            return redirect()->to('/');
        }

        $request->validate([
            "id" => 'required|integer|exists:App\Models\User,id'
        ]);

        $user = User::query()->find($request->id);

        $following= $user->following;

        return view('/follower', [
            'subscritons' => $following,
            'followers' => 1
        ]);
    }

    public function followUser(Request $request)
    {
        if (!Auth::check()) {
            http_response_code(401);
            return redirect()->to('/');
        }

        $request->validate([
            "id" => 'required|integer|exists:App\Models\User,id'
        ]);

        $subscription = new Subscription();
        $subscription->follower_id = Auth::user()->id; //пользователь, который подписывается на другого пользователя
        $subscription->following_id = $request->id;     //пользователь, на которого подписываются другие пользователи
        $subscription->save();

        return redirect()->back();
    }


    public function unfollowUser(Request $request)
    {
        if (!Auth::check()) {
            http_response_code(401);
            return redirect()->to('/');
        }

        $request->validate([
            "id" => 'required|integer|exists:App\Models\User,id'
        ]);

        Subscription::where('follower_id', '=', Auth::user()->id)
            ->where('following_id', '=', $request->id)
            ->delete();

        return redirect()->back();
    }
}
