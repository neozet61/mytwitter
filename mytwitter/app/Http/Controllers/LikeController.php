<?php

namespace App\Http\Controllers;

use App\Models\Like;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller
{
    public function like(Request $request)
    {
        if (!Auth::check()) {
            http_response_code(401);
            return redirect()->to('/');
        }

        $request->validate([
            "id" => 'required|integer|exists:App\Models\Tweet,id'
        ]);

        $like = new Like();
        $like->user_id = Auth::user()->id; //пользователь, который ставит Like
        $like->tweet_id = $request->id;     //твит, на который ставят Like
        $like->save();

        return redirect()->back();
    }


    public function dislike(Request $request)
    {
        if (!Auth::check()) {
            http_response_code(401);
            return redirect()->to('/');
        }

        $request->validate([
            "id" => 'required|integer|exists:App\Models\Tweet,id'
        ]);

        Like::where('user_id', '=', Auth::user()->id)   //пользователь, который ставит Like
            ->where('tweet_id', '=', $request->id)      //твит, на который ставят Like
            ->delete();

        return redirect()->back();
    }

}
