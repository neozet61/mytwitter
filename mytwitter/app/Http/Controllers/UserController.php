<?php

namespace App\Http\Controllers;

use App\Models\Like;
use App\Models\Subscription;
use App\Models\Tweet;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class UserController extends BaseController
{

    public function showProfile(Request $request)
    {
        $user = User::findOrFail($request->id);

        if (!Auth::check()) {
            http_response_code(401);
            return redirect()->to('/');
        }

        $subscription = Subscription::where('follower_id', '=', Auth::user()->id)
            ->where('following_id', '=', $user->id)
            ->get()
            ->first();

        $countFollowing = Subscription::where('follower_id', '=', $user->id)
            ->get()
            ->count();

        $countFollowers = Subscription::where('following_id', '=', $user->id)
            ->get()
            ->count();

        $tweets = Tweet::where('user_id', '=', $user->id)
            ->latest()
            /*->orderBy('created_at', 'DESC')*/
            ->get();

        $likes = Like::query()
            ->where('user_id', '=', $user->id)
            ->get();

        $likedTweetsIds = $likes->pluck('tweet_id')->all();

        $countTweets = $tweets->count();

        return view('profile', [
            'user' => $user,
            'tweets' => $tweets,
            'countTweets' => $countTweets,
            'subscription' => $subscription,
            'countFollowing' => $countFollowing,
            'countFollowers' => $countFollowers,
            'likedTweetsIds' => $likedTweetsIds
        ]);
    }


    public function settingsForm()
    {
        if (!Auth::check()) {
            http_response_code(401);
            return redirect()->to('/');
        }

        return view('settings');
    }


    public function store(Request $request)
    {
        $validatedData = $request->validate([
            "avatar" => 'nullable|image',
            "username" => 'nullable|string|max:50',
            "email" => 'nullable|string|email|unique:users',
            "password" => 'nullable|string|min:7',
            "bio" => 'nullable|string|max:250',
        ]);

        $user = User::query()->find(Auth::user()->id);

        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar')->store('public/images');
            $user->avatar = substr($avatar, 7);
        }

        if ($request->filled('username')) {
            $user->username = $validatedData['username'];
        }

        if ($request->filled('email')) {
            $user->email = $validatedData['email'];
        }

        if ($request->filled('password')) {
            $user->password = bcrypt($validatedData['password']);
        }

        if ($request->filled('bio')) {
            $user->bio = $validatedData['bio'];
        }

        $user->save();

        return redirect()->to('/profile?id=' . Auth::user()->id);
    }


    public function showExplore()
    {
        if (!Auth::check()) {
            http_response_code(401);
            return redirect()->to('/');
        }

        $popularUsers = User::query()
            ->selectRaw('users.*, COUNT(subscriptions.follower_id) AS followers_count')
            ->join('subscriptions', 'users.id', '=', 'subscriptions.following_id')
            ->groupBy('users.id')
            ->orderBy('followers_count', 'DESC')
            ->limit(3)
            ->get();

        $popularTweetsLastWeek = Tweet::query()
            ->selectRaw('tweets.*, COUNT(likes.tweet_id) as likes_count')
            ->join('likes', 'tweets.id', '=', 'likes.tweet_id')
            ->where('tweets.created_at', '>=', now()->subWeek())
            ->groupBy('tweets.id')
            ->orderByDesc('likes_count')
            ->limit(3)
            ->get();

        return view('explore', [
            'popularUsers' => $popularUsers,
            'popularTweetsLastWeek' => $popularTweetsLastWeek
        ]);
    }

    public function search(Request $request)
    {
        if (!Auth::check()) {
            http_response_code(401);
            return redirect()->to('/');
        }

        $request->validate([
            "search" => 'required|string|min:1|max:250',
        ]);

        $searchString = $request->search;

        $searchUsers = User::where(function ($query) use ($searchString) {
            $query->where('login', 'like', '%' . $searchString . '%')
                ->orWhere('username', 'like', '%' . $searchString . '%');
        })
            ->get();

        return view('/search-result', [
            'searchUsers' => $searchUsers,
            'searchString' => $searchString
        ]);
    }
}
