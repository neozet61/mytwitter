<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends BaseController
{


    public function showLoginForm()
    {

        return view('login');
    }

    public function store(Request $request)
    {
        $request->validate([
            "email" => 'required|string|email|exists:App\Models\User,email',
            "password" => 'required|string|min:7'
        ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect()->to('/home-feed');
        } else{
            return redirect()->back();
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate(); // Удаляем сессию

        $request->session()->regenerateToken(); // Обновляем CSRF-токен

        return view('welcome');
    }
}
