<?php

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class RegisterController extends BaseController
{

    public function showRegisterForm()
    {
        return view('register');
    }

    public function store(Request $request){
        $request->validate([
            "login" => 'required|string|max:50',
            "username"=> 'required|string|max:50',
            "email" => 'required|string|email|unique:App\Models\User,email',
            "password" => 'required|string|min:7|confirmed'
        ]);

        $user = new User();
        $user->login = "@" . $request->login;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        Auth::attempt(['email'=>$request->email, 'password'=>$request->password]);

        return redirect()->to('/');
    }
}
