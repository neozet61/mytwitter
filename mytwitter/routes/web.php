<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    if(Auth::check()){
        return redirect()->to('/home-feed');
    }else{
    return view('welcome');
    }
});

Route::redirect('/home', '/');

Route::get('/login', [\App\Http\Controllers\LoginController::class,'showLoginForm']);
Route::post('/login-save', [\App\Http\Controllers\LoginController::class,'store']);
Route::get('/logout', [\App\Http\Controllers\LoginController::class,'logout']);

Route::get('/register', [\App\Http\Controllers\RegisterController::class,'showRegisterForm']);
Route::post('/register-save', [\App\Http\Controllers\RegisterController::class,'store']);

Route::get('/profile', [\App\Http\Controllers\UserController::class,'showProfile'])->name('user.profile');
Route::get('/settings', [\App\Http\Controllers\UserController::class,'settingsForm']);
Route::post('/settings-save', [\App\Http\Controllers\UserController::class,'store']);
Route::get('/explore', [\App\Http\Controllers\UserController::class,'showExplore']);
Route::post('/search', [\App\Http\Controllers\UserController::class,'search']);


Route::get('/home-feed', [\App\Http\Controllers\TweetController::class,'showTweets']);
Route::post('/new-tweet', [\App\Http\Controllers\TweetController::class,'createTweet']);


Route::get('/followers', [\App\Http\Controllers\SubscriptionController::class,'showFollower']);
Route::get('/following', [\App\Http\Controllers\SubscriptionController::class,'showFollowing']);
Route::get('/follow', [\App\Http\Controllers\SubscriptionController::class,'followUser']);
Route::get('/unfollow', [\App\Http\Controllers\SubscriptionController::class,'unfollowUser']);


Route::get('/like', [\App\Http\Controllers\LikeController::class,'like']);
Route::get('/dislike', [\App\Http\Controllers\LikeController::class,'dislike']);

