@extends('layouts.base')

@section('title','Twitter-like Profile')

@section('content')

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">Profile</h3>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="profile-image">
                                @if($user->avatar == null)
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" fill="currentColor"
                                         class="bi bi-person-circle" viewBox="0 0 16 16">
                                        <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
                                        <path fill-rule="evenodd"
                                              d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
                                    </svg>
                                @else
                                    <img src="{{ asset('/storage/' . $user->avatar) }}"
                                         alt="Profile Image" class="img-fluid rounded-circle"
                                         width="100" height="100">
                                @endif
                            </div>
                            <div class="profile-info">
                                <h4>{{ $user->username }}</h4>
                                <p>{{  $user->login }}</p>
                            </div>
                        </div>
                        <p class="mt-3">
                            {{ $user->bio}}
                        </p>
                        <div class="d-flex justify-content-around">
                            <div class="tweets">
                                <h5>Tweets</h5>
                                <p>{{ $countTweets }}</p>
                            </div>
                            <div class="following">
                                <h5>Following</h5>
                                <p><a href="/following?id={{$user->id}}">{{$countFollowing}}</a></p>
                            </div>
                            <div class="followers">
                                <h5>Followers</h5>
                                <p><a href="/followers?id={{$user->id}}">{{$countFollowers}}</a></p>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        @if($user->id != Auth::user()->id)
                        @if(!$subscription)
                        <a href="/follow?id={{$user->id}}" class="btn btn-primary">Follow</a>
                        @else
                        <a href="/unfollow?id={{$user->id}}" class="btn btn-danger">Unfollow</a>
                        @endif
                        @endif
                    </div>
                </div>

                <!-- User's Tweets -->
                <div class="card mt-4">
                    <div class="card-header">
                        <h4 class="mb-0">Tweets</h4>
                    </div>
                    <ul class="list-group list-group-flush">
                        @foreach($tweets as $tweet)
                            <li class="list-group-item">
                                {{$tweet->content}}<span
                                    class="text-muted"> {{$tweet->created_at->diffForHumans()}}</span>
                                <div class="tweet-actions float-right">
                                    @if(!in_array($tweet->id, $likedTweetsIds))
                                        <a href="/like?id={{$tweet->id}}" class="ml-3"><i
                                                class="fas fa-heart mr-1"></i><span
                                                class="like-count">{{$tweet->likes->count()}}</span></a>
                                    @else
                                        <a href="/dislike?id={{$tweet->id}}" class="liked ml-3"><i
                                                class="fas fa-heart danger mr-1"></i><span
                                                class="like-count">{{$tweet->likes->count()}}</span></a>
                                    @endif
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
