@extends('layouts.base')

@section('title','Twitter-like Home Page')

@section('content')

    <div class="container mt-5">
        <!-- New Tweet Form -->
        <div class="row">
            <div class="col-md-6 mx-auto">
                @if($errors->any())
                    <div class="alert alert-danger small p-2">
                        <ul class="mb-0">
                            @foreach($errors->all() as $massage)
                                <li>{{$massage}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-body">
                        <form action="/new-tweet" method="post">

                            @csrf
                            <div class="form-group">
                                <label for="newTweet">What's happening?</label>
                                <textarea class="form-control" id="newTweet" rows="3" name="newTweet"
                                          placeholder="Write your tweet here..."></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Tweet</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- News Feed -->
        <div class="row mt-4">
            <div class="col-md-6 mx-auto">
                <div class="card">
                    <div class="card-header">
                        <h4 class="mb-0">News Feed</h4>
                    </div>
                    <ul class="list-group list-group-flush">
                        @if(!empty($tweets))
                            @foreach($tweets as $tweet)
                                <li class="list-group-item">
                                    <div class="tweet">
                                        @if(!$tweet->user->avatar == null)
                                            <img
                                                src="{{ asset('/storage/' . $tweet->user->avatar) }}"
                                                alt="User Image" class="img-fluid rounded-circle mr-2" width="40">
                                        @else
                                            <svg xmlns="http://www.w3.org/2000/svg" width="40" fill="currentColor"
                                                 class="bi bi-person-circle" viewBox="0 0 16 16">
                                                <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
                                                <path fill-rule="evenodd"
                                                      d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
                                            </svg>
                                        @endif
                                        <strong><a href="/profile?id={{$tweet->user_id}}"
                                                   class="text-decoration-none">{{$tweet->user->username}}</a></strong>
                                        <p>{{$tweet->content}}<span
                                                class="text-muted"> {{$tweet->created_at->diffForHumans()}}</span></p>
                                        <div class="tweet-actions float-right">
                                            @if(!in_array($tweet->id, $likedTweetsIds))
                                            <a href="/like?id={{$tweet->id}}" class="ml-3"><i
                                                    class="fas fa-heart mr-1"></i><span
                                                    class="like-count">{{$tweet->likes->count()}}</span></a>
                                            @else
                                                <a href="/dislike?id={{$tweet->id}}" class="liked ml-3"><i
                                                        class="fas fa-heart danger mr-1"></i><span
                                                        class="like-count">{{$tweet->likes->count()}}</span></a>
                                            @endif
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        @else
                            <li class="list-group-item">
                                <div class="tweet">
                                    <h4>Здесь будут твиты людей на которых Вы подпишитесь</h4>
                                </div>
                            </li>
                        @endif
                    </ul>
                </div>
                <!-- Pagination -->
                <nav aria-label="Page navigation example" class="mt-4">
                    {{$tweets->links()}}
                </nav>
            </div>
        </div>
    </div>

@endsection
