@extends('layouts.base')

@section('activeHome', '')

@section('title','Twitter-like Explore Page')

@section('activeExplore', 'active')

@section('content')
<div class="container mt-5">
    <div class="row">
        <!-- Popular Tweets -->
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0">Popular Tweets from Last Week</h4>
                </div>
                <ul class="list-group list-group-flush">
                    @foreach($popularTweetsLastWeek as $tweet)
                    <li class="list-group-item">
                        <strong>
                            <a href="/profile?id={{$tweet->user_id}}" class="text-decoration-none">
                                {{$tweet->user->username}}
                            </a>
                        </strong>
                        <p>{{$tweet->content}}<span
                                class="text-muted"> {{$tweet->created_at->diffForHumans()}}</span></p>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <!-- Popular Users -->
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0">Most Popular Users</h4>
                </div>
                <ul class="list-group list-group-flush">
                    @foreach($popularUsers as $user)
                    <li class="list-group-item">
                        @if(!$user->avatar == null)
                            <img
                                src="{{ asset('/storage/' . $user->avatar) }}"
                                alt="User Image" class="img-fluid rounded-circle mr-2" width="40">
                        @else
                            <svg xmlns="http://www.w3.org/2000/svg" width="40" fill="currentColor"
                                 class="bi bi-person-circle" viewBox="0 0 16 16">
                                <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
                                <path fill-rule="evenodd"
                                      d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
                            </svg>
                        @endif
                        <strong><a href="/profile?id={{$user->id}}" class="text-decoration-none">{{$user->username}}</a></strong>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
</div>

@endsection
