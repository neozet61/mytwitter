@extends('layouts.base')

@section('title','Twitter-like Follower')

@section('content')

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">
                            @if($followers == 1)
                                Followers
                            @else
                                Followings
                            @endif
                        </h3>
                    </div>
                    <ul class="list-group list-group-flush">
                        <!-- Example follower -->
                      {{--  @if(!empty($subscritons))--}}
                        @foreach($subscritons as $subscriber)
                            <li class="list-group-item">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="profile-image">
                                        @if(!$subscriber->avatar == null)
                                            <img
                                                src="{{ asset('/storage/' . $subscriber->avatar) }}"
                                                alt="User Image" class="img-fluid rounded-circle mr-2" width="48">
                                        @else
                                            <svg xmlns="http://www.w3.org/2000/svg" width="48" fill="currentColor"
                                                 class="bi bi-person-circle" viewBox="0 0 16 16">
                                                <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
                                                <path fill-rule="evenodd"
                                                      d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
                                            </svg>
                                        @endif
                                    </div>
                                    <div class="profile-info">
                                        <h5>{{$subscriber->username}}</h5>
                                        <p>{{$subscriber->login}}</p>
                                    </div>
                                    <a href="/profile?id={{$subscriber->id}}" class="btn btn-primary">View Profile</a>
                                </div>
                            </li>
                        @endforeach
                       {{-- @else
                            <li class="list-group-item">
                                <div class="d-flex justify-content-between align-items-center">
                                    <h4>Пока у пользователя нет подписок, но отображаться они будут тут!</h4>
                                </div>
                            </li>
                        @endif--}}
                        <!-- Example follower -->
                        <li class="list-group-item">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="profile-image">
                                    <img
                                        src="https://secure.gravatar.com/avatar/40d4f7caf1ddd071f41d949588fc3303?s=48&d=identicon"
                                        alt="User Image" class="img-fluid rounded-circle" width="48">
                                </div>
                                <div class="profile-info">
                                    <h5>Jane Doe</h5>
                                    <p>@janedoe</p>
                                </div>
                                <a href="user1.html" class="btn btn-primary">View Profile</a>
                            </div>
                        </li>
                        <!-- Example follower -->
                        <li class="list-group-item">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="profile-image">
                                    <img
                                        src="https://secure.gravatar.com/avatar/40d4f7caf1ddd072f41d949588fc3303?s=48&d=identicon"
                                        alt="User Image" class="img-fluid rounded-circle" width="48">
                                </div>
                                <div class="profile-info">
                                    <h5>Bob Smith</h5>
                                    <p>@bobsmith</p>
                                </div>
                                <a href="user2.html" class="btn btn-primary">View Profile</a>
                            </div>
                        </li>

                        <!-- Example follower -->
                        <li class="list-group-item">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="profile-image">
                                    <img
                                        src="https://secure.gravatar.com/avatar/40d4f7caf1ddd071f41d949588fc2303?s=48&d=identicon"
                                        alt="User Image" class="img-fluid rounded-circle" width="48">
                                </div>
                                <div class="profile-info">
                                    <h5>Alice Johnson</h5>
                                    <p>@alicejohnson</p>
                                </div>
                                <a href="user3.html" class="btn btn-primary">View Profile</a>
                            </div>
                        </li>

                        <!-- Add more followers as needed -->
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
