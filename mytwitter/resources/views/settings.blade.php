@extends('layouts.base')

@section('title','Twitter-like User Settings')

@section('content')

<div class="container mt-5">
    <h3 class="mb-4">User Settings</h3>
    <form action="/settings-save" method="post" enctype="multipart/form-data">
        @if($errors->any())
            <div class="alert alert-danger small p-2">
                <ul class="mb-0">
                    @foreach($errors->all() as $massage)
                        <li>{{$massage}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @csrf
        <div class="form-group">
            <label for="avatar">Profile picture</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="avatar" name="avatar">
                <label class="custom-file-label" for="avatar">Choose file</label>
            </div>
        </div>
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" class="form-control" id="username" name="username" placeholder="Enter username"
                   value="{{ Auth::user()->username }}">
        </div>
        <div class="form-group">
            <label for="email">Email address</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Enter email"
                   {{--value="{{ Auth::user()->email }}--}}">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password"
                   placeholder="Enter new password">
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea class="form-control" id="bio" rows="3" name="bio"
                      placeholder="Enter a short bio"
            >@if(!Auth::user()->bio == null) {{ Auth::user()->bio }} @endif</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Save Changes</button>
    </form>
</div>
@endsection
