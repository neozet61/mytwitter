@extends('layouts.base')

@section('activeHome', '')

@section('activeExplore', '')

@section('title','Twitter-like Search')

@section('content')

<div class="container mt-5">
    <h3 class="mb-4">Search Results for "{{$searchString}}"</h3>
    <div class="row">
        <!-- Searched Profiles -->
        <div class="col-md-12">
            <div class="list-group">
                @foreach($searchUsers as $searchUser)
                <a href="/profile?id={{$searchUser->id}}" class="list-group-item list-group-item-action">
                    @if(!$searchUser->avatar == null)
                        <img
                            src="{{ asset('/storage/' . $searchUser->avatar) }}"
                            alt="User Image" class="img-fluid rounded-circle mr-2" width="40">
                    @else
                        <svg xmlns="http://www.w3.org/2000/svg" width="40" fill="currentColor"
                             class="img-fluid rounded-circle mr-2" viewBox="0 0 16 16">
                            <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
                            <path fill-rule="evenodd"
                                  d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
                        </svg>
                    @endif
                    <strong>{{$searchUser->username}} {{$searchUser->login}}</strong>
                </a>
                @endforeach
            </div>
        </div>
    </div>
</div>

@endsection
